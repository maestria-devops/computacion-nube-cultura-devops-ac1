module "vm_gcp" {
  source = "../../vm-gcp"

  project_id           = "master-devops-2023"
  region               = "us-central1"
  main_zone            = "us-central1-a"
  environment          = "act1"
  bastion_machine_type = "e2-micro"
}
